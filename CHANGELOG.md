## [0.1.1] - 2021-09-22

### Changed

- Changed licence field to valid SPDX license
- Updated dev dependencies

## [0.1.0] - 2020-04-21

### Added
- Initial release. Added `IpReserved.is_reserved?/1` that works on IPv4 & IPv6 with String form or Tuple.  
