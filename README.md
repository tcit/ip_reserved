# IpReserved

IpReserved is a tool that checks if the given IP is private / reserved or "normal". Works with IPv4 and IPv6 addresses.

[Documentation](https://hexdocs.pm/ip_reserved)
[![](https://img.shields.io/hexpm/v/ip_reserved.svg)](https://hex.pm/packages/ip_reserved)

## Installation

The package can be installed
by adding `ip_reserved` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:ip_reserved, "~> 0.1.1"}
  ]
end
```

# Usage

Pass IP as a string to know if it's reserved
```elixir
iex(1)> IpReserved.is_reserved?("192.168.0.1")
true
```

You can also pass the IP as a tuple
```elixir
iex(2)> IpReserved.is_reserved?({8, 8, 1, 1})
false
```

IPv6 addresses are also supported.
```elixir
iex(3)> IpReserved.is_reserved?("2001:db8:0:85a3::ac1f:8001")
true
iex(4)> IpReserved.is_reserved?({8193, 3512, 0, 34211, 0, 0, 44063, 32769})
true
```
