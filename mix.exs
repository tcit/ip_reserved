defmodule IpReserved.MixProject do
  use Mix.Project

  def project do
    [
      app: :ip_reserved,
      version: "0.1.1",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      description:
        "A tool that checks if the given IP is private / reserved or \"normal\". Works with IPv4 and IPv6 addresses.",
      deps: deps(),
      package: package(),
      test_coverage: [
        tool: ExCoveralls
      ],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test
      ],
      name: "IP Reserved",
      source_url: "https://framagit.org/tcit/ip_reserved",
      homepage_url: "https://framagit.org/tcit/ip_reserved",
      docs: [
        # The main page in the docs
        main: "IpReserved",
        extras: ["README.md", "CHANGELOG.md"]
      ],
      dialyzer: [
        plt_file: {:no_warn, "priv/plts/dialyzer.plt"}
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:inet_cidr, "~> 1.0.0"},
      {:credo, "~> 1.4", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.0", only: [:dev], runtime: false},
      {:ex_doc, "~> 0.21", only: :dev, runtime: false},
      {:excoveralls, "~> 0.10", only: :test},
      {:doctor, "~> 0.18.0", only: :dev}
    ]
  end

  defp package() do
    [
      files: ~w(lib .formatter.exs mix.exs README* LICENSE* CHANGELOG*),
      licenses: ["GPL-3.0-only"],
      links: %{
        "Framagit" => "https://framagit.org/tcit/ip_reserved"
      }
    ]
  end
end
