defmodule IpReservedTest do
  use ExUnit.Case
  doctest IpReserved

  describe "ipv4" do
    test "works with basic private ips" do
      assert IpReserved.is_reserved?("192.168.0.1") == true
    end

    test "works with basic public ips" do
      assert IpReserved.is_reserved?("8.8.8.8") == false
    end

    test "works with tuple form" do
      assert IpReserved.is_reserved?({1, 1, 1, 1}) == false
    end
  end

  describe "ipv6" do
    test "works with ipv6" do
      assert IpReserved.is_reserved?("2607:f0d0:1002:51::4") == false
    end

    test "works with private ipv6" do
      assert IpReserved.is_reserved?("fdd5:2767:5895:47c2::1") == true
    end

    test "works with tuple form" do
      assert IpReserved.is_reserved?({8193, 3512, 0, 34_211, 0, 0, 44_063, 32_769}) == true
    end
  end
end
